Installation Dual Boot Windows Debian
=====================================
Sous Windows
------------
Réduire la partition C: de 100 Go
 - navigateur de fichier
 - clic droit sur ordinateur
 - gérer
 - stockage
 - gestion des disques
Noter l’adresse IP configurée
 - cmd > ipconfig
 - ex. 172.16.25.111 / 16 , gw, dns : 172.16.0.1

Au démarrage ( BIOS / EFI )
---------------------------
Démarrer sous d-i
# Au lieu de tout configurer sur chaque poste, je sers le fichier suivant
# à l’adresse http://172.16.25.42/d-i/buster/./preseed.cfg construit avec :

	grep -v -e ^# -e ^$ preseed-pirae-comments.txt > preseed.cfg

 - F12 en T15, DEL en T17, F8 en T12
 - UEFI Mass storage
 - Help
 - boot: autogui url=172.16.25.42
 - adresse IP : 172.16.2…/16 (cf. configuration Windows)
 - passerelle et DNS : 172.16.0.1

Sous Debian
-----------
Certains paramétrages sont à faire sur toutes les machines, ClusterSSH
permet de se connecter rapidement partout pour le faire.

# La première fois : cssh -o'-o PubkeyAuthentication=no' eleve@…

cssh 172.16.25.{{100..104},10{6,7},11{0..7},119,180,248} # T15
cssh 172.16.25.{4,8,9,11,{13..22},24,99} # T17
cssh 172.16.2.{1..14} # T12
cssh 172.16.15.1{{01..08},11,12,{14..16},{18..20},{22..26},{28..30},33,35,37,89} # T6
cssh 172.16.2.1{01..18} #T12, le retour


# Configurer le proxy au niveau système
dconf write /system/proxy/mode "'manual'"
# la suivante ne passe pas le C&P sous cssh, de pas coller les crochets…
dconf write /system/proxy/ignore-hosts "['localhost', '127.0.0.0/8', '::1', '172.16.0.0/16']"
dconf write /system/proxy/http/host "'172.16.0.1'"
dconf write /system/proxy/http/port "3128"
dconf write /system/proxy/https/host "'172.16.0.1'"
dconf write /system/proxy/https/port "3128"

# Evince par défaut pour ouvrir les PDF
$ cat ~eleve/.config/mimeapps.list
[Default Applications]
application/pdf=org.gnome.Evince.desktop

[Added Associations]
application/pdf=org.gnome.Evince.desktop;

# Les commandes suivantes nécessitent d’être superutilisateur
su -

# Windows en démarrage par défaut
# la suivante ne passe pas le C&P sous cssh, de pas coller les tirets bas…
mv /etc/grub.d/30_os-prober /etc/grub.d/08_os-prober
sed -i s/=5/=10/ /etc/default/grub
update-grub

# Ajout du miroir local
# les suivantes ne passent pas le C&P sous cssh, de pas coller les chevrons, les tubes, ni les esperluettes…
# echo 'Acquire::http::Proxy "http://172.16.25.42:3142/";' > /etc/apt/apt.conf
# echo 'deb http://172.16.25.42/archive/debian/ buster main' >> /etc/apt/sources.list
# wget http://172.16.25.42/archive/conf/pirae.gpg -O - | apt-key add -
apt update && apt upgrade && apt install pirae-students

# Correction de la disposition de clavier (il y a un problème dans le preseed…)
dpkg-reconfigure keyboard-configuration # choisir Frans AZERTY…

# Finir la configuration de wireshark et unattended-upgrades
DEBIAN_FRONTEND=noninteractive dpkg-reconfigure wireshark-common unattended-upgrades
addgroup eleve wireshark

# Corriger le nom de la machine
vi /etc/hostname

# Un compte personnel sudoer pour administrer les machines plus facilement
adduser taffit # (les paramètres qui vont bien pour mode non interactif ?)
addgroup taffit sudo
su taffit
mkdir --mode=700 /home/taffit/.ssh
# les deux suivantes ne passent pas le C&P sous cssh, de pas coller les tirets bas…
wget http://172.16.25.123/archive/apt/conf/id_rsa_pirae.pub -O - > /home/taffit/.ssh/authorized_keys
chmod 644 /home/taffit/.ssh/authorized_keys
chown -R taffit: /home/taffit/.ssh

# Copie de /srv/SNT et lien qui va bien
rsync …
ln -s /srv/SNT ~eleve/Documents/

# autossh sur les postes prof pour permettre de se connecter de l’extérieur…


# LDAP 172.16.0.3
# scribe.lpt.lan

Plan des salles
---------------

T15 (18 postes)
~~~
172.16.25.1xx
mil: 117
gch: 119, 115, 114, 113
fnd: 112, 111, 110, 248, 180, 107, 106, 116
drt: 104, 103, 102, 101
prf: 100 (same hardware as T12)

T17 (16 postes)
~~~
172.16.25.xx
sens direct à partir de la T15 :
fnd: 11, 8, 14, 9, 4, 13, [20], [15]
gch: 16, 18, 19, 21
drt: 22, 17, 99
prf: 24

T12 (14 postes)
~~~
172.16.2.xx
Sens direct à partir de la porte :
gch : 5, 4[abs], 3, 2, 1,
prf : 14
dte : 13, 12, 11, 10, 9, 8, 7, 6

Renewal
172.16.2.1xx
0{4..6},10,{12..18}

T6 (28 postes sous Debian)
~~~
/!\ pci=noaer pour les trois ordi récents (37, 89, 35)
172.16.15.1xx
prf : 01
fnt : 37, 89, 35, 24, 23, 03, 05, 06, 22, 08, 04, 16, [32 : smallDD, no Debian]
mur : 11, 07, 15, 14, 20, 18, 19, 33
il1 : 30, 28
il2 : 26, 12, 25, 29, 02
