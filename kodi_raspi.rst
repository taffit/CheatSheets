# https://raspi.debian.net/how-to-image/
# Copie d’une image vers la carte SD branchée sur l’ordinateur
# /!\ remplacer « TRUC » par b (si c’est bien /dev/sdb)
xzcat ~/Debian/iso/20200831_raspi_3.img.xz | sudo dd of=/dev/sdTRUC bs=64k oflag=dsync status=progress

# Montage des partition dans /media/taffit/RASPI{FIRM,ROOT} en mode graphique

# Configuration de base dans un chroot
sudo mount --bind /dev /media/taffit/RASPIROOT/dev/
sudo mount --bind /dev/pts /media/taffit/RASPIROOT/dev/pts/
sudo mount --bind /media/taffit/RASPIFIRM /media/taffit/RASPIROOT/boot/firmware
sudo chroot /media/taffit/RASPIROOT
# Modification du mot de passe du superutilisateur
passwd

# Utilisation du cache APT
echo 'Acquire::http::Proxy "http://persil:3142/";' > /etc/apt/apt.conf.d/80proxy
# Configuration du réseau (DNS)
echo 'search lan
nameserver 192.168.1.254
nameserver 9.9.9.9' > /etc/resolv.conf
# Configuration du WiFi
echo 'allow-hotplug wlan0
iface wlan0 inet dhcp
    wpa-ssid WIFI_MaBox_4CC849
    wpa-psk 165CEDC16B' > /etc/network/interfaces.d/wlan0
# Ajout d’allocation mémoire au GPU (non permanent, éditer aussi /etc/kernel/postinst.d/z50-raspi3-firmware, faire un rapport de bug…)
echo '[all]
gpu_mem=256' >> /boot/firmware/config.txt 
# Correction des erreurs CMA
sed -i s/cma=64M/cma=512M/ /boot/firmware/cmdline.txt
echo CMA=512M >> /etc/default/raspi3-firmware
# Renommer la machine
echo kodi > /etc/hostname
# Mise à jour de la liste des paquets (pas des paquets eux-mêmes par manque de place)
apt update
# Ajout de quelques outils pour faciliter l’administration
apt install screen sudo vim-nox

# Ajout d’un utilisateur pour SSH
adduser taffit
addgroup taffit sudo
addgroup taffit adm

# Configuration de SSH
mkdir /home/taffit/.ssh
echo 'ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAABgQCxLYPTcqZFR2nBIsKBjO6+uPDWRg4a2KKr+HdsLgS4mylID4iMiYM8B8RYxZetfs6L/5Be9qSaSh68GkUIis9sMN6RnCikk0C3d5ig9ucHBmu9yeoB1EEFnTP5tfNc1yZpv1fEvVm/hyM/703HqWS8eUBV5W4EKow3f4IcmrkP40YpfJ6Sv8UQqOGEJ0a9Gqig0WrbQ+wU2X17J4TKNYHYLfxfPhJcxZLQMh5Mcx2Z9i/pDO2ltMj+hIpXQUgD5NeVFXjfeap4wG5SKDRXahXUMU0Po1Zrkbn/h3LNk7INyGlS96fxScYbTq19Pt8MS/kOz2lVbH9IfYymlY9tU/lMsuaXMjvc/p1qd6nGSHmGIOBEcz7+9QdjjDyCddu0vfLyqbDYhA+AsUldwDKz7eVbEIBOBT+Iwp6ZF/A7m2ZBdep0f7t0mK5NZoigUVfimBMt4633p3Rj9atVZQxyaiozqYJvvLxbeVE82Y7kC7aqD4IeCiFOmkXsl6rKeGwoixc= taffit@persil' >> /home/taffit/.ssh/authorized_keys
chmod 700 /home/taffit/.ssh/
chown -R taffit: /home/taffit/.ssh/
# Ajout d’un utilisateur pour kodi
adduser --disabled-password kodi

# Configuration pour démarrage automatique
echo '#!/bin/sh
kodi' >> ~kodi/.xsession
chown kodi: ~kodi/.xsession


# La partition d’origine est trop petite pour l’installation du reste.
# Sortir du chroot
exit
# Démonter les partitions
sudo umount /media/taffit/RASPIROOT/{boot/firmware,dev/{pts/,}}
umount /media/taffit/RASPI*
# Déconnecter la carte SD 


# Connecter la carte SD dans le raspberry PI et le démarrer
# Se connecter en SSH
ssh kodi
# Ouvrir un terminal « durable »
screen
# Mise à jour des paquets
sudo apt update && sudo apt upgrade

# https://appuals.com/how-to-make-home-theater-using-raspberry-pi/
# https://kodi.wiki/view/HOW-TO:HOW-TO:Autostart_Kodi_for_Linux
# Installation de Kodi (avec
#  - nodm pour la connexion automatique
#  - xinit pour utiliser le clavier en USB
#  - nfs-common pour les partages NFS)
sudo apt install kodi nodm xinit nfs-common

# Configuration de nodm pour démarrer avec l’utilisateur kodi
sudo dpkg-reconfigure nodm
