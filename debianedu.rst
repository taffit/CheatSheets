# https://bugs.debian.org/939717
/!\ Nom en ASCII pendant l’installation (pas d’accent)…

Paramétrage supplémentaire
--------------------------
/etc/network/interfaces pour eth0 (réseau de lycée) et dummy0 (en 10.0.2.2):
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
# https://wiki.debian.org/DebianEdu/HowTo/ChangeIpSubnet
# https://justus.berlin/2018/08/dummy-network-device-in-ubuntu-linux-18-04/
auto eth0
iface eth0 inet static
    address 172.16.25.42
    netmask 255.255.0.0
    broadcast 172.16.255.255
    gateway 172.16.0.1

auto dummy0
iface dummy0 inet static
    address 10.0.2.2
    netmask 255.255.255.255
    pre-up rmmod dummy; modprobe dummy numdummies=4

Forwarder DNS
-------------
/etc/bind/named.conf.options

Proxy HTTP(S) Squid
-------------------
sudo sh -c 'echo "cache_peer 172.16.79.254 parent 3128 0 no-query no-digest no-netdb-exchange default
http_access allow all
never_direct allow all
forwarded_for transparent" > /etc/squid/conf.d/acajou.conf'

## Autre idée, squizer squid…
## sed -i s/webcache/172.16.79.254/ /etc/…
##         /etc/debian-edu/www/wpad.dat
##         /etc/environment
##         /var/lib/tftpboot/debian-edu/install.cfg ?
##         /etc/debian-edu/pxeinstall.conf ?

Activer le NAT
--------------
sudo sh -c 'echo "NETWORK_TO_NAT=10.0.0.0/8
OUTSIDE_IF=wlan0" > /etc/default/enable-nat'

autossh pour se connecter de l’extérieur (et purge ou configuration de killer)
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
$ crontab -l
@reboot autossh -fNt -R 19990:localhost:22 -D 1080 carotte
$ cat ~/.ssh/config
Host carotte
Hostname carotte.tilapin.org
Port 2468
User stidd
KeepAlive yes
ProtocolKeepAlives 60
Compression yes
ServerAliveInterval 25
(avec la clef SSH pour se connecter à carotte)

Drop killer that kills autossh…
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
sudo apt purge killer

apt-cacher-ng pour remplacer squid (on ne cache que des paquets, il y a déjà un proxy sur le réseau du lycée)
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
sudo sh -c 'echo Proxy: http://webcache:3128 > /etc/apt-cacher-ng/proxy.conf'
sudo sh -c 'echo Acquire::http::Proxy "http://webcache:3142"; > /etc/apt/apt.conf'

reprepro pour le miroir local (nsi-students et ses dépendances)
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
# https://wiki.debian.org/DebianRepository/SetupWithReprepro

gpg --gen-key
sudo apt install reprepro
sudo mkdir --parents /srv/archive/debian
sudo chown -R taffit: /srv/archive
sudo sh -c 'cat << EOF > /etc/apache2/conf-available/archive.conf
Alias /archive /srv/archive

<Directory /srv/archive/ >
        # We want the user to be able to browse the directory manually
        Options Indexes FollowSymLinks Multiviews
        Require all granted
</Directory>

# This syntax supports several repositories, e.g. one for Debian, one for Ubuntu.
# Replace * with debian, if you intend to support one distribution only.
<Directory "/srv/archive/*/db/">
        Require all denied
</Directory>
<Directory "/srv/archive/*/conf/">
        Require all denied
</Directory>
<Directory "/srv/archive/*/incoming/">
        Require all denied
</Directory>
EOF'
sudo a2enconf archive
sudo systemctl reload apache2
mkdir /srv/archive/debian/conf

sudo sh -c "cat << EOF > /srv/archive/debian/conf/distributions
Origin: NSI
Label: NSI-All
Suite: stable
Codename: buster
Version: 10
Architectures: i386 amd64 source
Components: main
Description: NSI high school packages
SignWith: `gpg -K | grep -P '[0-9A-F]{10}' | sed 's/ //g'`
EOF"

sudo sh -c 'echo "verbose
basedir /srv/archive/debian
ask-passphrase" > /srv/archive/debian/conf/options'

REPREPRO_BASE_DIR=/srv/archive/debian/ reprepro include buster nsi_0~~20201006_amd64.changes
wget https://www.lernsoftware-filius.de/downloads/Setup/filius_1.10.3_all.deb
# et aussi : notepadqq_2.0.0~beta1-1~bpo10+1_amd64.deb
REPREPRO_BASE_DIR=/srv/archive/debian/ reprepro --ignore=wrongdistribution includedeb buster *deb

mkdir /srv/archive/conf
gpg --armor --output /srv/archive/conf/nsi.gpg --export `gpg -K | grep -P '[0-9A-F]{10}' | sed 's/ //g'`

wget -O - http://tjener/archive/conf/nsi.gpg | sudo apt-key add -

sudo sh -c 'echo "deb http://tjener/archive/debian/ buster main
deb-src http://tjener/archive/debian/ buster main" >> /etc/apt/sources.list.d/nsi.list'
