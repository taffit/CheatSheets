Copie d’image
=============
unzip -p ~/Debian/iso/2020-08-20-raspios-buster-armhf-full.zip | sudo dd of=/dev/sdTRUC bs=64k oflag=dsync status=progress

Activation SSH
==============
source : https://www.raspberrypi.org/documentation/remote-access/ssh/

Directement sur la carte SD

  $ sudo touch <boot>/SSH

Une fois connecté (pour préserver ce comportement), ou via chroot

  $ sudo raspi-config

Attribution adresse IP statique en cas de DHCP non disponible
=============================================================
Directement sur la carte sd, ou une fois connecté

  $ sudo vi <root>/etc/dhcpcd.conf

cf. logique à la fin du fichier :

|  # It is possible to fall back to a static IP if DHCP fails:
|  # define static profile
|  profile static_eth0
|  static ip_address=192.168.1.100/24
|  static routers=192.168.1.254
|  static domain_name_servers=192.168.1.254
|  
|  # fallback to static profile on eth0
|  interface eth0
|  fallback static_eth0

Configuration du WiFi en point d’accès
======================================
sources : https://www.raspberrypi.org/documentation/configuration/wireless/access-point.md
          https://frillip.com/using-your-raspberry-pi-3-as-a-wifi-access-point-with-hostapd/

Une fois connecté

  $ sudo apt update
  $ sudo apt upgrade
  $ sudo apt install dnsmasq hostapd
  $ sudo vi /etc/network/interfaces.d/wlan0

Dans ce fichier :

|  allow-hotplug wlan0
|      iface wlan0 inet static
|      address 192.168.4.1
|      netmask 255.255.255.0
|      network 192.168.4.0
|      broadcast 192.168.4.255
|  #    wpa-conf /etc/wpa_supplicant/wpa_supplicant.conf

  $ sudo vi /etc/hostapd/hostapd.conf

Dans ce fichier :

|  interface=wlan0
|  driver=nl80211
|  ssid=Routeur
|  hw_mode=g
|  channel=7
|  wmm_enabled=1
|  macaddr_acl=0
|  auth_algs=1
|  ignore_broadcast_ssid=0
|  wpa=2
|  wpa_passphrase=RouteurManu
|  wpa_key_mgmt=WPA-PSK
|  wpa_pairwise=TKIP
|  rsn_pairwise=CCMP

  $ sudo vi /etc/default/hostapd

dans ce fichier :

|  DAEMON_CONF="/etc/hostapd/hostapd.conf"

  $ sudo vi /etc/dnsmasq.d/wlan0

dans ce fichier :

|  interface=wlan0
|    dhcp-range=192.168.4.2,192.168.4.20,255.255.255.0,24h
